// 2.7.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string.h>

int _tmain(int argc, _TCHAR* argv[])
{
	char a;
	int b[256], i;
	memset(b, 0, sizeof(b));
	for(;;)
	{
		scanf("%c", &a);
		if(a==10) break;
		b[a]++;
	}
	for(i=0;i<256;i++)
		if(b[i]>0)
			printf("%c %d\n", i, b[i]);
	return 0;
}

