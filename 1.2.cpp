// 1.2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

int _tmain(int argc, _TCHAR* argv[])
{
	int h, m, s;
	puts("Enter current time in format hh:mm:ss");
	scanf("%d:%d:%d", &h, &m, &s);
	if(h>23 || h<0 || m>59 || m<0 || s>59 || s<0)
	{
		puts("Date has been entered wrong.");
		return 0;
	}
	if(h>=3 && h<=11) puts("Good morning!");
	else if(h>11 && h<16) puts("Good day!");
	else if(h>=16 && h<21) puts("Good evening!");
	else puts("Good night!");
	return 0;
}

