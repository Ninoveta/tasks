// 3.5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <time.h>
#include <stdlib.h>

int _tmain(int argc, _TCHAR* argv[])
{
	long int const N=10;
	long int i, a[N], p=-1, n=-1, s=0;
	srand(time(0));
	while(p==-1 || n==-1)
	{
		for(i=0;i<N;i++)
		{
			a[i]=rand()%10001-5000;
			if(a[i]>0 && p==-1) p=i;
			else if(a[i]<0 && n==-1) n=i+1;
		}
	}
	for(i=n;i<p;i++) s+=a[i];
	printf("%ld\n", s);
	return 0;
}

