// 3.8.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string.h>

int _tmain(int argc, _TCHAR* argv[])
{
	int i=0, inWord=0, count=0, n;
	char str[256];
	puts("Enter a string and a number, please");
	fgets(str, 256, stdin);
	scanf("%d", &n);
	str[strlen(str)-1]=32;
	while(str[i])
	{
		if(str[i]!=32 && inWord==0)
		{
			count++;
			if(count==n) break;
			inWord=1;
		}
		else if(str[i]==32 && inWord==1)
			inWord=0;
		i++;
	}
	if(!str[i]) { puts("You entered the incorrect number"); return 0; }
	n=i;
	while(str[n]!=32)
	{
		putchar(str[n]);
		n++;
	}
	puts("");
	return 0;
}

