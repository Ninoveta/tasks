// 6.2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

int _tmain(int argc, _TCHAR* argv[])
{
	unsigned long int n, max=0, i, j, N;
	for(n=2;n<1000001;n++)
	{
		i=n; j=1;
		while(i>1)
		{
			i=(i&1)?(3*i+1):(i/2);
			j++;
		}
		if(j>max) { max=j; N=n; }
	}
	printf("%ld\n", N);
	return 0;
}

