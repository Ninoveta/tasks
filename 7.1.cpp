// 7.1.cpp : Defines the entry point for the console application.
// 7.1_1.txt

#include "stdafx.h"
#include <string.h>
#include <stdlib.h>

struct tnode {
char *word; //��������� �� �����
int count; //����� ���������
struct tnode *left; //����� ���������
struct tnode *right; //������ ���������
};

struct tnode *addtree(struct tnode *p, char *w)
{
	//addtree: ��������� ����� w � ���� p ��� ���� ����
	int cond;
	if(p==NULL)
	{
		//��������� ������ ��� ����� ����
		p=(struct tnode *)malloc(sizeof(struct tnode));
		p->word=strdup(w); //strdup: �������� ������ � �������� � �� ������
		p->count=0;
		p->left=p->right=NULL;
	}
	//else if((cond=strcmp(w, p->word))==0)
	//	p->count++;
	//��� ����� ��� �����������
	else if((cond=strcmp(w, p->word))<0) //������ ����� ������ ���������
		p->left=addtree(p->left, w);
	else //������ ����� ������� ���������
		p->right=addtree(p->right, w);
	return p;
}

void runtree(struct tnode *p, char *w)
{
	//addtree: ��������� ���� w � ���� p ��� ���� ����
	int cond;
	/*if(p==NULL)
	{
		//��������� ������ ��� ����� ����
		p=(struct tnode *)malloc(sizeof(struct tnode));
		p->word=strdup(w); //strdup: �������� ������ � �������� � �� ������
		p->count=0;
		p->left=p->right=NULL;
	}*/
	if(p!=NULL)
	{
		if(!(cond=strcmp(w, p->word)))
			p->count++; //��� ����� ��� �����������
		else if(cond<0) //������ ����� ������ ���������
			//p->left=addtree(p->left, w);
			runtree(p->left, w);
		else //������ ����� ������� ���������
			//p->right=addtree(p->right, w);
			runtree(p->right, w);
		//return p;
	}
}

void treeprint(struct tnode *p)
{
	//treeprint: ������������� ������ ������ p
	if(p!=NULL)
	{
		treeprint(p->left); //������ ������ ���������
		printf("%d %s\n", p->count, p->word); //������ ����
		treeprint(p->right); //������ ������� ���������
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	char a[33][20]={"for", "break", "switch", "char", "unsigned", "continue", "do",
	"signed", "enum", "float", "auto", "goto", "if", "default", "int", "long", "return",
	"short", "sizeof", "case", "static", "const", "switch", "double", "typedef", "union", "extern", "void",
	"volatile", "register", "while", "struct", "else"}, b[1000];
	int i, f=0, j;
	struct tnode *root=NULL; //root - ������ ������
	FILE *fp;
	for(i=0;i<33;i++)
		root=addtree(root, a[i]);
	fp=fopen("7.1_1.txt", "rt");
	if(fp==NULL)
	{
		perror("File error");
		return 1;
	}
	for(;;)
	{
		i=fgetc(fp);
		if(i!=10 && i!=32 && i!=9 && i!=EOF)
		{
			if(f)
			{
				j++;
				b[j]=i;
			}
			else
			{
				f=1; j=0;
				b[j]=i;
			}
		}
		else if(f)
		{ 
			f=0; j++; b[j]=0;
			runtree(root, b);
		}
		else if(i==EOF) break;
	}
	treeprint(root);
	fclose(fp);
	return 0;
}
