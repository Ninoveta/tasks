// 2.5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <time.h>

int _tmain(int argc, _TCHAR* argv[])
{
	int i, j;
	char a;
	srand(time(0));
	for(i=0;i<10;i++)
	{
		for(j=0;j<8;j++)
		{
			switch(rand()%3)
			{
			case 0:
				a=rand()%10+48;
				break;
			case 1:
				a=rand()%26+65;
				break;
			case 2:
				a=rand()%26+97;
				break;
			}
			putchar(a);
		}
		puts("");
	}
	return 0;
}

