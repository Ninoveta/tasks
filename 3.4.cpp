// 3.4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

int _tmain(int argc, _TCHAR* argv[])
{
	char a;
	long long int b=0, f=0, c=0;
	do
	{
		scanf("%c", &a);
		if(a>47 && a<58 && f<9)
		{
			b=(f)?b*10+a-48:a-48;
			f++;
		}
		else if(a>47 && a<58) { f=1; c+=b; b=a-48; }
		else { f=0; c+=b; b=0; }
	}
	while(a!=10);
	printf("%lld\n", c);
	return 0;
}

