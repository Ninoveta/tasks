// 3.10.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string.h>

int _tmain(int argc, _TCHAR* argv[])
{
	int i=0, inWord=0, count=0, n, f=-1, g=-1;
	char str[256];
	puts("Enter a string and a number, please");
	fgets(str, 256, stdin);
	scanf("%d", &n);
	str[strlen(str)-1]=32;
	for(;;)
	{
		if(str[i]!=32 && inWord==0)
		{
			count++;
			if(count==n && f<0) f=i;
			else if(f>=0) { g=i; break; }
			inWord=1;
		}
		else if(str[i]==32 && inWord==1)
			inWord=0;
		if(!str[i]) break;
		i++;
	}
	if(f<0) { puts("You entered the incorrect number"); return 0; }
	i=-1;
	do
	{
		i++;
		str[f+i]=str[g+i];
	}
	while(str[g+i]);
	puts(str);
	return 0;
}

