// 6.3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

int _tmain(int argc, _TCHAR* argv[])
{
	long long int n, i=-1;
	char a[20];
	puts("Enter a number");
	scanf("%lld", &n);
	for(;;)
	{
		i++;
		a[i]=n%10+48;
		if(n<=9) break;
		n/=10;
	}
	for(n=i;n>=0;n--) putchar(a[n]);
	puts("");
	return 0;
}

