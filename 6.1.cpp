// 6.1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string.h>

char a[27][27];

void star(int x, int y, int n)
{
	if(n==1) { a[x][y]=42; a[x-1][y]=42; a[x][y-1]=42; a[x+1][y]=42; a[x][y+1]=42; }
	else if(n==2) { star(x+3, y, 1); star(x, y+3, 1); star(x-3, y, 1); star(x, y-3, 1); }
	else 
		{ 
			star(x+9, y, 1); star(x, y+9, 1); star(x-9, y, 1); star(x, y-9, 1); 
			star(x+9, y, 2); star(x, y+9, 2); star(x-9, y, 2); star(x, y-9, 2); 
		}
}

int _tmain(int argc, _TCHAR* argv[])
{
	int i, j;
	memset(a, 32, sizeof(a));
	for(i=1;i<4;i++)
		star(13, 13, i);
	for(i=0;i<27;i++)
	{
		for(j=0;j<27;j++)
			putchar(a[i][j]);
		puts("");
	}
	return 0;
}

