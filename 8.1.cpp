// 8.1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string.h>
#include <stdlib.h>

struct SYM
{
	unsigned char ch; // ASCII-���
	float freq; // ������� �������������
	char code[256]; // ������ ��� ������ ����
	struct SYM *left; // ����� ������� � ������
	struct SYM *right; // ������ ������� � ������
}syms[256];
SYM *psysms[256];

struct SYM* buildTree(struct SYM *psym[], int N)
{
struct SYM *temp=(struct SYM*)malloc(sizeof(struct SYM));
temp->freq=psym[N-2]->freq+psym[N-1]->freq;
temp->left=psym[N-1];
temp->right=psym[N-2];
temp->code[0]=0;
if(N==2) return temp;
int i;
for(i=N-3;i>=0;i--)
{
	if(psym[i]->freq>=temp->freq) break;
	psym[i+1]->freq=psym[i]->freq;
}
psym[i+1]->freq=temp->freq;
return buildTree(psym, N-1);
}

void makeCodes(struct SYM *root)
{
	if(root->left)
	{
		strcpy(root->left->code, root->code);
		strcat(root->left->code, "0");
		makeCodes(root->left);
	}
	if(root->right)
	{
		strcpy(root->right->code, root->code);
		strcat(root->right->code, "1");
		makeCodes(root->right);
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	int i, j, k=0;
	float t;
	unsigned char c;
	FILE *fp;
	memset(syms, 0, sizeof(syms));
	fp=fopen("8.1_1.txt", "rt");
	if(fp==NULL)
	{
		perror("File error");
		return 1;
	}
	while((i=fgetc(fp))!=EOF)
	{
		k++;
		syms[i].ch=i; syms[i].freq++;

	}
	for(i=0;i<255;i++)
		for(j=0;j<255-i;j++)
			if(syms[j].freq<syms[j+1].freq)
			{
				t=syms[j].freq; syms[j].freq=syms[j+1].freq; syms[j+1].freq=t;
				c=syms[j].ch; syms[j].ch=syms[j+1].ch; syms[j+1].ch=c;
			}
	
	i=0;
	while(syms[i].freq>0)
	{
		syms[i].freq/=k;
		syms[i].left=NULL; syms[i].right=NULL;
		printf("%lc %f\n", syms[i].ch, syms[i].freq);
		psysms[i]=(struct SYM *)malloc(sizeof(struct SYM));
		psysms[i]=&syms[i];
		i++;
	}
	//buildTree(psysms, i);
	makeCodes(buildTree(psysms, i));
	for(j=0;j<i;j++)
	{
		printf("%f\n", psysms[j]->freq);
	}
	fclose(fp);
	return 0;
}
