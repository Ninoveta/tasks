// 3.6.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <time.h>
#include <stdlib.h>

int _tmain(int argc, _TCHAR* argv[])
{
	long int const N=10;
	long int i, a[N], s=0, max=-5000, min=5000, p, n;
	srand(time(0));
	for(i=0;i<N;i++)
	{
		a[i]=rand()%10001-5000;
		if(a[i]>max) { p=i; max=a[i]; }
		if(a[i]<min) { n=i; min=a[i]; }
	}
	if(n>p) { i=n; n=p; p=i; }
	for(i=n+1;i<p;i++) s+=a[i];
	printf("%ld\n", s);
	return 0;
}
