// 2.2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <time.h>

int _tmain(int argc, _TCHAR* argv[])
{
	int a, b;
	srand(time(0));
	a=rand()%100+1;
	do
	{
		scanf("%d", &b);
		if(a<b) puts("<");
		else if(a>b) puts(">");
	}
	while(a!=b);
	puts("Guessed!");
	return 0;
}

