// 3.2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

int _tmain(int argc, _TCHAR* argv[])
{
	int i=0, inWord=0, count=0;
	char a;
	puts("Enter a string, please:");
	do
	{
		scanf("%c", &a);
		if(a!=32 && a!=10)
		{
			printf("%c", a); i++;
			if(inWord==0)
			{
				count++;
				inWord=1;
			}
		}
		else if((a==32 || a==10) && inWord==1)
		{
			inWord=0;
			printf(" %d\n", i); i=0;
		}
	}
	while(a!=10);
	printf("Number of words: %d\n", count);
	return 0;
}

